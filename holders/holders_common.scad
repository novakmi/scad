/**
* Common reusable modules to implement various clip, box holders.
* (c) 2019-2023 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

/**
Make rotated hole (fer screw) of given diameter.
d ... diameter
w ....width
*/
module screw_hole(d, w) {
    rotate([00,90,00]) {cylinder(d=d,w);}
}

/**
Make clip cylinder of given diameter height and width.
d ... diameter
h ... height
w ... width
ct ... fine tune cutting
*/
module clip_cylinder(d, h, w, ct=0) {
    difference () {
        cylinder(d=d+w*1.5,h=h);
        cylinder(d=d,h=h);
        translate([d*0.7+ct,0,0]) cylinder(d=d+w,h=h);

    }
}

/**
Make wall touching box.
l ... length
h ... height
w ... width
left_ext ... extention to the left (before left hole)
right_ext ... extension to the right (before right hole)
hole_diameter ... hole diameter
hole_offset ... how much to add to hole positions (from left and right)
left_hole ... make left hole ?
right_hole ... make right holde ?
*/
module wall_box(l, h, w, left_ext=0, right_ext=0, hole_diameter = 0, hole_offset=0,
                left_hole=true, right_hole=true) {
    //echo(right_ext);
    difference() {
        translate([0,-left_ext,0]) cube(size=[w,left_ext+l+right_ext,h]);
        if (left_hole) {
            //echo("Making left hole");
            translate([0,-left_ext+hole_diameter+hole_offset,h/2]) screw_hole(hole_diameter, w);
        }
        if(right_hole) {
            //echo("Making right hole");
            translate([0, right_ext+l-hole_diameter-hole_offset,h/2]) screw_hole(hole_diameter,w);
        }
    }
}

/**
Make box holder of given diameter height and width.
l ... length
h ... height
w ... width
t ... thicknes
cut ... cut in the bottom
*/
module box_holder(l,h,w,t, cut_l=0, cut_w=0) {
    difference() {
        cube(size=[w+2*t,l+2*t,h+t]);
        translate([t,t,t]) cube(size=[w,l,h]);
        translate([(w+2*t-cut_w)/2,(l+2*t-cut_l)/2,0]) cube([cut_w, cut_l, t]);
    }
}

/**
Make clip.
d ... clip diameter
l ... length (of box touching wall, clip is in the middle)
h ... height
w ... width
move_y ... move on y axis
left_ext ... extention to the left (before left hole)
right_ext ... extension to the right (before right hole)
hole_diameter ... hole diameter
hole_offset ... how much to add to hole positions (from left and right)
left_hole ... make left hole ?
right_hole ... make right holde ?
ct ... fine tune cutting
*/
module clip(d, l, h, w, move_y=0, left_ext=0, right_ext=0,
            hole_diameter = 0, hole_offset=0, left_hole=true, right_hole=true, ct=0) {
    translate([0,move_y ,0]) {
    translate([0,0,0]) wall_box(l=l, h=h, w=w, left_ext=left_ext, right_ext=right_ext,
             hole_diameter=hole_diameter, hole_offset=hole_offset,
             left_hole=left_hole, right_hole=right_hole);
    translate([d/2+w*.95,l/2,0]) clip_cylinder(d=d, w=w, h=h, ct=ct);
    }
}


/**
Make clip with sides.
d ... clip diameter
l ... length (of box touching wall, clip is in the middle)
h ... height
w ... width
move_y ... move on y axis
left_ext ... extention to the left (before left hole)
right_ext ... extension to the right (before right hole)
hole_diameter ... hole diameter
hole_offset ... how much to add to hole positions (from left and right)
left_hole ... make left hole ?
right_hole ... make right holde ?
ct ... fine tune cutting
*/
module clip_with_sides(d, l, h, w, move_y=0, left_ext=0, right_ext=0,
            hole_diameter = 0, hole_offset=0, left_hole=true, right_hole=true, ct=0, top=true, bottom = true) {
    translate([0,move_y,0]) {

        wall_box(l=l, h=h, w=w, left_ext=left_ext, right_ext=right_ext,
                hole_diameter=hole_diameter, hole_offset=hole_offset,
                left_hole=left_hole, right_hole=right_hole);
        translate([d/2+w*.95,l/2,0]) {
            difference() {
                group () {
                    clip_cylinder(d=d, w=w, h=h, ct=ct);
                    cy=2*d/2*sin(400);
                    if (bottom) {
                        cylinder(d=d+w, h=w);
                        translate ([-d/2-w*0.9,0-cy/2,0]) cube(size=[3*w,/*d-3*w*/cy, w]);
                    }
                    if (top) translate([0,0,h-w]) {
                        cylinder(d=d+w, h=w);
                        translate ([-d/2-w,-d/2+1.5*w,0]) cube(size=[3*w,d-3*w, w]);
                    }
                }
                if (bottom) translate([0,0,w]) cylinder(d=d+2*w, h=w/2);
                if (top) translate([0,0,h-1.5*w]) cylinder(d=d+2*w, h=w/2);
            }
        }
    }
}


/**
Make box holder of given diameter height and width.
l ... length (including wall_box, 2xthicness)
h ... height
w ... width (including 2xthickness)
t ... thicknes
move_y ... move on y axis
wall_h ... wall height
wall_h_offset ... offset of wall mount
wall_ext ... wall extension on one side
left_ext ... extention to the left (before left hole)
right_ext ... extension to the right (before right hole)
hole_diameter ... hole diameter
hole_offset ... how much to add to hole positions (from left and right)
left_hole ... make left hole ?
right_hole ... make right holde ?
cut_l ... length of botom cutout
cut_w ... width of botom cutout
*/
module box(l,h,w,t,wall_h=0,wall_h_offset=0,wall_ext=0, move_y=0, left_ext=0, right_ext=0,
        hole_diameter = 0, hole_offset=0, left_hole=true, right_hole=true,
        cut_l=0, cut_w=0) {
    box_l=l-2*(wall_ext+t);
    translate([0,move_y,0]) {
        translate ([0,0,wall_h_offset]) wall_box(l=l, h=wall_h, w=t, left_ext=left_ext, right_ext=right_ext,
          hole_diameter=hole_diameter, hole_offset=hole_offset,
          left_hole=left_hole, right_hole=right_hole);
        translate([0, l/2-box_l/2-t,0]) box_holder(box_l,h,w,t, cut_l=cut_l, cut_w=cut_w);
    }
}


//https://openhome.cc/eGossip/OpenSCAD/SectorArc.html

module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360])
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

/**
* radius
* angles = [a1, a2]  .... a1 first arc angle, a2 second arc angle (from 0 counterclockwise)
* h ... height
* width
* fn ... steps (points) in arc
*/
module arc(radius, angles, h, width = 1, fn = 24) {
    linear_extrude(h) {
        difference() {
            sector(radius + width, angles, fn);
            sector(radius, angles, fn);
        }
    }
}
