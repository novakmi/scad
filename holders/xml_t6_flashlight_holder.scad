/**
* Clip holder for XML-T6 flashlight (sold on ebay, aliexpress).
* (c) 2020 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height=19; //19mm - sticker tape
hole_diameter=3;
hole_offset=2;
width=3;
xml_dia=26; // width of XML-T6 -1
small_xml_dia=23;  // width of smaller XML-T6 -1
dia=xml_dia;
holes=true;

include <holders_common.scad>

module holder() {
    le = 0;
    d1=dia; l1=d1+20; y_next1=l1+le;
    clip(d=d1, l=l1, h=height, w=width, move_y=0, left_ext=le,
         hole_diameter=hole_diameter,hole_offset=hole_offset,
         left_hole=holes, right_hole=false);
    d2=dia; l2=l1; y_next2=y_next1+l2;
    clip(d=d2, l=l2, h=height, w=width, move_y=y_next1,
         hole_diameter=hole_diameter, hole_offset=hole_offset,
         left_hole=false, right_hole=holes);
//     d3=dia; l3=l1; y_next3=y_next2+l2;
//     clip(d=d3, l=l3, h=height, w=width, move_y=y_next2,
//          hole_diameter=hole_diameter, hole_offset=hole_offset,
//          left_hole=false, right_hole=holes);
}

holder();