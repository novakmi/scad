/**
* Clip holder for Alfawise H6S vacuum cleaner accessories.
* (c) 2019 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height=15;
hole_diameter=4;
width=4;

include <holders_common.scad>

module holder() {
    d1=27; l1=d1+10*hole_diameter; y_next1=l1;
    clip(d=d1, l=l1,  h=height, w=width, move_y=0,
         hole_diameter=hole_diameter, right_hole=false);
    d2=30; l2=d2+10*hole_diameter; y_next2=y_next1+l2;
    clip(d=d2, l=l2,  h=height, w=width, move_y=y_next1,
        hole_diameter=hole_diameter, left_hole=false, right_hole=false);
    d3=31; l3=d3+10*hole_diameter; y_next3=y_next2+l3;
    clip(d=d3, l=l3, h=height, w=width,move_y=y_next2,
         hole_diameter=hole_diameter, left_hole=false, right_hole=true);
}

holder();