/**
* Clip holder for nasal pump spray bottle (sold on ebay).
* (c) 2019 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height=10;
hole_diameter=3;
hole_offset=2;
width=4;

include <holders_common.scad>

module holder() {
    le = 0;
    d1=21; l1=d1+20; y_next1=l1+le;
    clip(d=d1, l=l1, h=height, w=width, move_y=0, left_ext=le,
         hole_diameter=hole_diameter,hole_offset=hole_offset,
         right_hole=false);
    d2=21; l2=l1; y_next2=y_next1+l2;
    clip(d=d2, l=l2, h=height, w=width, move_y=y_next1,
         hole_diameter=hole_diameter, hole_offset=hole_offset,
         left_hole=false);
}

holder();