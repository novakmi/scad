/**
* Box holder for toothpaste.
* (c) 2020 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height=50;
width = 35;
small_height=30;
small_width=28;
thick=3;

include <holders_common.scad>

module holder() {
    l1=27+1*thick; w1=small_width; h1=small_height; t=thick;
    box_holder(l1,h1,w1,t, cut_l=0.5*l1, cut_w=0.4*w1);
    l2=34*4+2*thick; w2=width; h2=height;
    translate([0,l1+thick,0]) box_holder(l2,h2,w2,t, cut_l=0.9*l2, cut_w=0.6*w2);
    translate([0,l1+l2+2*thick,0]) box_holder(l1,h1,w1,t, cut_l=0.3*l1, cut_w=0.3*w1);
}

holder();