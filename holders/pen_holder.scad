/**
* Clip holder for pens
* (c) 2020 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height=8; //half of doubel side sticker sticker tape
hole_diameter=2; //hole for thin screws
hole_offset=0; //move hole to edge (-) or to the center (+); e.g. -0.5
width=2; // thickness
dia=10; // set diameter of pen/pencil (regular pen 10, pencil 8, ...)
holes=true; // add screw holes to the end?

include <holders_common.scad>

module holder() {
    le = 0;
    d1=dia; l1=d1+10; y_next1=l1+le;
    clip(d=d1, l=l1, h=height, w=width, move_y=0, left_ext=le,
         hole_diameter=hole_diameter,hole_offset=hole_offset,
         left_hole=holes, right_hole=false);
    d2=dia; l2=l1; y_next2=y_next1+l2;
    clip(d=d2, l=l2, h=height, w=width, move_y=y_next1,
         hole_diameter=hole_diameter, hole_offset=hole_offset,
         left_hole=false, right_hole=false);
   d3=dia; l3=l1; y_next3=y_next2+l3;
   clip(d=d3, l=l3, h=height, w=width, move_y=y_next2,
        hole_diameter=hole_diameter, hole_offset=hole_offset,
        left_hole=false, right_hole=false);
   d4=dia; l4=l1; y_next4=y_next3+l4;
   clip(d=d4, l=l4, h=height, w=width, move_y=y_next3,
        hole_diameter=hole_diameter, hole_offset=hole_offset,
        left_hole=false, right_hole=false);
   d5=dia; l5=l1; y_next5=y_next4+l5;
   clip(d=d5, l=l5, h=height, w=width, move_y=y_next4,
        hole_diameter=hole_diameter, hole_offset=hole_offset,
        left_hole=false, right_hole=holes);
    // add more clips if needed
}

holder();