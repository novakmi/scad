/**
* Box holder for Alfawise H6S vacuum cleaner accessories.
* (c) 2019 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height=15;
hole_diameter=3;
thick=3;

include <holders_common.scad>

module holder() {
    wall_ext=10;
    re = 10;
    l1=47+2*(wall_ext+thick); w1=33; h1=height; t=thick; y_next1=l1-re;
    box(l1,h1,w1,t, wall_h=15, wall_h_offset=3, wall_ext=wall_ext, right_ext=0,
        hole_diameter=hole_diameter, cut_l=40, cut_w=20, right_hole=false);
    l2=47+2*(thick+wall_ext); w2=w1; h2=height; y_next2=y_next1+l2-re;
    box(l2,h2,w2,t, wall_h=15, move_y=y_next1, wall_h_offset=3, wall_ext=wall_ext,
        hole_diameter=hole_diameter, cut_l=46, cut_w=32, left_hole=false, right_hole=false);
    l3=45+2*(thick+wall_ext); w3=28; h3=height+10; y_next3=y_next2+l3;
    box(l3,h3,w3,t, wall_h=15, move_y=y_next2, wall_h_offset=3, wall_ext=wall_ext,
        hole_diameter=hole_diameter, cut_l=40, cut_w=20, left_hole=false);
}

holder();