/**
* Box holder for toothpaste.
* (c) 2023 Michal Novak, it.novakmi@gmail.com
* License: MIT
*/

height = 37;
thick = 1.5;
width = 8.5;
length = 8.5;
n = 8;

include <holders_common.scad>

module holder() {
    box_holder(length, height, width, thick, cut_l = 0.5 * length, cut_w = 0.5 * width);
}

module interdental_holder1() {
    for (i = [0 : n - 1]) {
        translate([0, (i) * (length + thick), 0]) holder();
    }
}

module pick_holder() {
    box_holder(length, 0, width / 3, thick, cut_l = 0.45 * length, cut_w = 0.3 * width);
}

module hook() {
    cube(size = [width / 3, thick, thick]);
    translate([width / 2.5, 0, 0]) rotate([0, - 90, 0]) cube(size = [width / 2, thick, thick]);
}

module interdental_holder2() {
    interdental_holder1();
    for (i = [0 : 3]) {
        translate([width + 2 * thick, (i) * (length + thick/1.5), 0]) pick_holder();
    }
    translate([width + 2 * thick, 5 * (length + thick), 0]) hook();
    translate([width + 2 * thick, 7 * (length + thick), 0]) hook();
}

module interdental_holder() {
    interdental_holder2();
}

//text = "dental care";
text = "";
union() {
    interdental_holder();
    translate([width + 1.5 * thick, (length + thick) * n / 2, height / 1.5]) rotate([90, 0, 90]) {
        linear_extrude(1)
            text(text, font = "Arial", size = 4, halign = "center");
    }
}
